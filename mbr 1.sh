!/bin/bash
cfdisk /dev/sda
mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mount /dev/sda2 /mnt
mkdir /mnt/boot
mount /dev/sda2 /mnt/boot
pacstrap -i /mnt base linux linux-firmware sudo nano
genfstab -U -p /mnt >> /mnt/etc/fstab
nano /etc/locale.gen
locale-gen
echo muriz > /etc/hostname
nano /etc/hosts
pacman -S networkmanager
passwd
pacman -S grub
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
exit
umount -R /mnt
reboot
passwd muriz
EDITOR=nano visudo
exit